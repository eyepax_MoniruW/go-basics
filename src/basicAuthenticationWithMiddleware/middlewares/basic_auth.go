package middlewares

import (
	"fmt"
	"net/http"
)

func BasicAuth(handler http.HandlerFunc) http.HandlerFunc  {
	return func(response http.ResponseWriter, request *http.Request) {
		username,password,ok:=request.BasicAuth()
		fmt.Println("Username", username)
		fmt.Println("Password",password)
		fmt.Println("ok",ok)

		if !ok || !checkUsernameAndPassword(username,password) {
			response.Header().Set("WWW-Authenticate",`Basic-realm="Account Invalid`)
			response.WriteHeader(401)
			response.Write([]byte("Unauthorised\n"))
			fmt.Println("Unauthorised")
			return
		}
		handler (response,request)
	}
}

func checkUsernameAndPassword(username,password string) bool {
	return username=="admin" && password == "123"
}