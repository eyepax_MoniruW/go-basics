package basicAuthenticationWithMiddleware

import (
	"./apis"
	"./middlewares"
	"fmt"
	"github.com/gorilla/mux"
	"net/http"
)

func BasicAuthenticationMiddlewareDemo()  {
	router:=mux.NewRouter()
	router.Handle("/api/product/findAll",middlewares.BasicAuth(apis.FindAll)).Methods("GET")
	router.Handle("/api/product/search",middlewares.BasicAuth(apis.Search)).Methods("GET")
	err := http.ListenAndServe(":3000",router)

	if err!=nil {
		fmt.Println(err)
	}
}