package middlewares

import (
	"fmt"
	"net/http"
)

func Middleware1(h http.Handler) http.Handler  {
	return http.HandlerFunc(func(response http.ResponseWriter,request *http.Request) {
		fmt.Println("Middleware 1: ",request.URL)
		h.ServeHTTP(response,request)
	})
}
