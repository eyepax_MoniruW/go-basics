package middleware

import (
	"fmt"
	"github.com/gorilla/mux"
	"./middlewares"
	"net/http"
	"./apis"
)

func MiddlewareDemo()  {
	router :=mux.NewRouter()
	//router.Handle("/api/demo1",middlewares.Middleware1(http.HandlerFunc(apis.Demo1API))).
	//	Methods("GET")
	//router.Handle("/api/demo2",middlewares.Middleware1(http.HandlerFunc(apis.Demo2API))).
	//	Methods("GET")
	router.Handle("/api/demo3",middlewares.Middleware1(middlewares.Middleware2(http.HandlerFunc(apis.Demo2API))) ).
		Methods("GET")
	err := http.ListenAndServe(":3000",router)

	if err!= nil {
		fmt.Println (err)
	}
}


