package polymorphism

import (
	"../entities"
	"fmt"
)

func PolymorphismDemo()  {
	Demo1()
}

func Demo1()  {
	circle1 := entities.Circle{"Circle 1",5}
	circle2 := entities.Circle{"Circle 2",2}
	square1 := entities.Square{"Square 1",5}
	square2 := entities.Square{"Square 2",9}
	rectangle1 := entities.Rectangle{"Rectangle 1",2,4}
	rectangle2 := entities.Rectangle{"Rectangle 1",7,3}
	geometries :=[]entities.Geometry{circle1,circle2,square1,square2,rectangle1,rectangle2}

	for _,geometry :=range geometries {
		fmt.Println(geometry.Type())
		fmt.Println("Area : ",geometry.Area())
		fmt.Println("Perimeter : ",geometry.Perimeter())
		fmt.Println("------------")
	}
}
