package pointers

import (
	"../entities"
	"fmt"
)

func PointersDemo () {
	fmt.Println("----Pointers----")
	Demo1()
	Demo2()
	Demo3()
	Demo4()
	Demo5()
	Demo6()
	Demo7()
}

func Demo1() {
	fmt.Println("---Demo 1---")
	var a int = 123
	var p *int =&a
	fmt.Println("Address of a:",&a)
	fmt.Println("Value of a: ",a)
	fmt.Println("Address of p:  ",p)
	fmt.Println("Value of p:",*p)
}

func Demo2() {
	fmt.Println("---Demo 2---")
	a:=456
	p:=&a
	fmt.Println("Address of a:",&a)
	fmt.Println("Value of a: ",a)
	fmt.Println("Address of p:  ",p)
	fmt.Println("Value of p:",*p)
}

func Change1(a int)  {
	fmt.Println("---Change 1---")
	a=2
	fmt.Println("value a changed to",a)
}

func Change2(b *int)  {
	fmt.Println("---Change 2---")
	*b=3
	fmt.Println("value b changed to",*b)
}

func Demo3()  {
	fmt.Println("---Demo 3---")
	a:=1
	fmt.Println("value a :",a)
	Change1(a)
	fmt.Println("a : ",a)
	b:=1
	fmt.Println("value b :",b)
	Change2(&b)
	fmt.Println("b : ",b)
}

func Swap1(a,b int)  {
	fmt.Println("---Swap 1---")
	temp :=a
	a=b
	b=temp
}

func Swap2(p,q *int)  {
	fmt.Println("---Swap 2---")
	temp :=*p
	*p=*q
	*q=temp
}

func Demo4()  {
	fmt.Println("---Demo 4---")
	a,b:=1,2
	fmt.Println("Before Swap 1 --> a: ",a," , b: ",b)
	Swap1(a,b)
	fmt.Println("a : ",a," , b : ",b)
	c,d:=3,4
	fmt.Println("Before Swap 2 --> c: ",c," , d: ",d)
	Swap2(&c,&d)
	fmt.Println("c : ",c," , d : ",d)
}

func Demo5()  {
	fmt.Println("---Demo 5---")
	var product entities.ProductPointers
	product.Id = "p1"
	product.Name = "Product 1"
	product.Price = 100
	fmt.Println(product)
	var p *entities.ProductPointers =&product
	fmt.Println("Product Info")
	fmt.Println("Id: ",(*p).Id)
	fmt.Println("Name: ",(*p).Name)
	fmt.Println("Price: ",(*p).Price)
}

func Demo6()  {
	fmt.Println("---Demo 6---")
	product :=entities.ProductPointers{
		Id:    "p2",
		Name:  "Product 2",
		Price: 50,
	}
	p:=&product
	fmt.Println(product)
	fmt.Println("Product Info")
	fmt.Println("Id: ",(*p).Id)
	fmt.Println("Name: ",(*p).Name)
	fmt.Println("Price: ",(*p).Price)
}

func ChangeProduct1(product entities.ProductPointers)  {
	fmt.Println("---Change Product 1---")
	product.Name ="def"
}

func ChangeProduct2(p *entities.ProductPointers)  {
	fmt.Println("---Change Product 2---")
	(*p).Name ="def"
}

func Demo7()  {
	fmt.Println("---Demo 7---")
	product := entities.ProductPointers{
		Id:    "p3",
		Name:  "product 3",
		Price: 450,
	}
	ChangeProduct1(product)
	fmt.Println(product)
	fmt.Println("Use Pointer")
	ChangeProduct2(&product)
	fmt.Println(product)
}
