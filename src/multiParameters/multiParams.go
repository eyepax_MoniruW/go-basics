package multiParameters

import "fmt"

//Multi parameters
func MultiSum (parameters ...int) int {
	fmt.Println("-----------------------")
	fmt.Println("Multiple Parameter SUM")
	fmt.Println("Size -",len(parameters))
	s:=0
	for _, v:= range parameters{
		s+=v
	}
	fmt.Println("Sum of parameters :",s)
	return s
}
