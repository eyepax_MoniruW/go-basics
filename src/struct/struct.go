package _struct

import (
	"../entities"
	"fmt"
	"strings"
)

func StructDemo () {
	fmt.Println("-----------------------")
	fmt.Println("Struct Demo")
	var songs [2] entities.Song
	songs[0] =entities.Song{
		Id:     1,
		Name:   "Song 1",
		Melody: entities.Melody{
			Owner:         "Owner 1",
			ContactNumber: 123,
		},
	}

	songs[1] =entities.Song{
		Id:     2,
		Name:   "Song 2",
		Melody: entities.Melody{
			Owner:         "Owner 2",
			ContactNumber: 234,
		},
	}

	newSong := entities.Song{
		Id:     3,
		Name:   "Song 3",
		Melody: entities.Melody{
			Owner:         "Owner 3",
			ContactNumber: 345,
		},
	}

	fmt.Println(newSong)
	fmt.Println(songs)
	fmt.Println(songs[1].Id)
	fmt.Println(search(songs,"Song 2"))
}

func search(songss [2] entities.Song, keyword string) (result []entities.Song) {
	fmt.Println("-----------------------")
	fmt.Println("Search the keyword :",keyword)
	for _, song:=range songss {
		if strings.Contains(strings.ToLower(song.Name),strings.ToLower(keyword)) {
			result =append(result,song)
		}
	}
	return
}