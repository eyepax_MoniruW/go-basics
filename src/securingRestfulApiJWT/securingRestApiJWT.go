package securingRestfulApiJWT

import (
	"./apis/accountapi"
	"fmt"
	"github.com/gorilla/mux"
	"net/http"
	"./middlewares"
	"./apis/demoapi"
)

func SecuringRestfulApiWitjJWT()  {
	router :=mux.NewRouter()
	router.HandleFunc("/api/account/generatekey",accountapi.GenerateToken).Methods("POST")
	router.HandleFunc("/api/account/checktoken",accountapi.CheckToken).Methods("GET")
	router.Handle("/api/demo/demo1",middlewares.JWTAuth(http.HandlerFunc(demoapi.Demo1))).Methods("GET")
	router.Handle("/api/demo/demo2",middlewares.JWTAuth(http.HandlerFunc(demoapi.Demo2))).Methods("GET")

	err :=http.ListenAndServe(":8000",router)

	if err != nil{
		fmt.Println("abc",err)
	}
}

