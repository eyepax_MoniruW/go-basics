package accountapi

import (
	"../../../entities"
	"encoding/json"
	"fmt"
	"github.com/dgrijalva/jwt-go"
	"net/http"
	"time"
)

var secretKey ="MySecretkey"

func GenerateToken(response http.ResponseWriter,request *http.Request)  {
	var account entities.AccountJWT

	err:= json.NewDecoder(request.Body).Decode(&account)

	if err != nil {
		respondWithError(response,http.StatusBadRequest,err.Error())
	} else {
		token:= jwt.NewWithClaims(jwt.SigningMethodHS256,jwt.MapClaims{
			"username" :account.Username,
			"password" :account.Password,
			"exp" :time.Now().Add(time.Hour*72).Unix(),
		})

		tokenString,err2 := token.SignedString([]byte(secretKey))

		if err2 != nil {
			respondWithError(response,http.StatusBadRequest,err2.Error())
		} else {
			respondWithJson(response,http.StatusOK,entities.JWTToken{Token:tokenString})
		}
	}
}

func CheckToken(response http.ResponseWriter,request *http.Request)  {
	tokenstring :=request.Header.Get("key")
	result,err := jwt.Parse(tokenstring, func(token *jwt.Token) (interface{}, error) {
		return []byte(secretKey),nil
	})
	if err == nil && result.Valid {
		fmt.Println("Valid")
	} else {
		fmt.Println("Invalid")
	}

}

func respondWithError(writer http.ResponseWriter, code int, msg string) {
	respondWithJson(writer,code, map[string]string{"error":msg})
}

func respondWithJson(writer http.ResponseWriter, code int, payload interface{}) {
	response,_:= json.Marshal(payload)
	writer.Header().Set("Content-type", "application/json")
	writer.WriteHeader(code)
	writer.Write(response)
}