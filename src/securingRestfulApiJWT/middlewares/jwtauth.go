package middlewares

import (
	"encoding/json"
	"github.com/dgrijalva/jwt-go"
	"net/http"
)

var secretKey ="MySecretkey"

func JWTAuth(next http.Handler) http.Handler  {
	return http.HandlerFunc(func(response http.ResponseWriter,request *http.Request) {
		tokenString :=request.Header.Get("key")

		if tokenString == "" {
			respondWithError(response,http.StatusUnauthorized,"Unauthorized")
		} else {
			result,err :=jwt.Parse(tokenString, func(token *jwt.Token) ( interface{},  error) {
				return []byte(secretKey),nil
			})

			if err == nil && result.Valid {
				next.ServeHTTP(response,request)
			} else {
				respondWithError(response,http.StatusUnauthorized,"Unauthorized")
			}
		}
	})
}

func respondWithError(writer http.ResponseWriter, code int, msg string) {
	respondWithJson(writer,code, map[string]string{"error":msg})
}

func respondWithJson(writer http.ResponseWriter, code int, payload interface{}) {
	response,_:= json.Marshal(payload)
	writer.Header().Set("Content-type", "application/json")
	writer.WriteHeader(code)
	writer.Write(response)
}