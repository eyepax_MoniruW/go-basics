package dateAndTime

import (
	"fmt"
	"time"
)

func DateAndTimeDemo()  {
	fmt.Println("---Date And Time---")
	Demo1()
	Demo2()
	Demo3()
	Demo4()
	Demo5()
}

func  Demo1() {
	fmt.Println("---Demo 1---Current Time---")
	today := time.Now()
	fmt.Print("Today :",today)
	year :=today.Year()
	month :=today.Month()
	day :=today.Day()
	hour :=today.Hour()
	minutes :=today.Minute()
	second := today.Second()
	fmt.Println("Year : ",year)
	fmt.Println("Month : ",month)
	fmt.Println("Number of the month : ",int(month))
	fmt.Println("Day : ",day)
	fmt.Println("Hour : ",hour)
	fmt.Println("Minutes : ",minutes)
	fmt.Println("Second : ",second)
}

func Demo2() {
	fmt.Println("---Demo 2---Format Date and Time---")
	today :=time.Now()
	fmt.Println("Format date :",today.Format("01/02/2006"))
	fmt.Println("Format time :",today.Format("15:04:05"))
	fmt.Println("Format date and time :",today.Format("01/02/2006 15:04:05"))
}

func Demo3()  {
	fmt.Println("---Demo 3---Parse Date and Format---")
	s:="10/20/1980"
	value,err:= time.Parse("01/02/2006",s)
	if err != nil {
		fmt.Println(err)
	} else {
		fmt.Println("new date: ",value.Format("2006-01-02"))
	}
}

func Demo4()  {
	fmt.Println("---Demo 4---Day Difference--")
	date1 :=time.Date(2019,time.Month(01),03,0,0,0,0,time.UTC)
	date2 :=time.Date(2019,time.Month(01),05,0,0,0,0,time.UTC)
	days :=date2.Sub(date1).Hours()/24
	fmt.Println("Days : ",days)
}

func Demo5()  {
	fmt.Println("---Demo 5---Add date---")
	today := time.Now()
	fmt.Println("Today Date :",today)
	newDate1 :=today.AddDate(0,0,2)
	fmt.Println("New Date 1 :",newDate1)
	newDate2 :=today.Add(2*24*time.Hour)
	fmt.Println("New Date 2 :",newDate2)
}
