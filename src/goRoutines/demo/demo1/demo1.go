package demo1

import (
	"fmt"
	"time"
)

func hello()  {
	fmt.Println("Hello")
}

func hi()  {
	fmt.Println("Hi")
}

func Run()  {
	go hello()
	go hi()
	time.Sleep(1*time.Second)
	fmt.Println("Done")
}