package demo3

import (
	"fmt"
	"time"
)

func displayNumbers()  {
	for i:=1;i<=20;i++ {
		fmt.Printf(" %d",i)
		time.Sleep(100 *time.Millisecond)
	}
}

func displayCharacters()  {
	for ch:='a';ch <= 'z';ch++ {
		fmt.Printf(" %c",ch)
		time.Sleep(100*time.Millisecond)
	}
}

func Run()  {
	go displayNumbers()
	go displayCharacters()
	time.Sleep(3000*time.Millisecond)
	fmt.Print(" Done ")
}
