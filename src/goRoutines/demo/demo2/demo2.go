package demo2

import (
	"fmt"
	"time"
)

func process1() {
	fmt.Println("Start Process 1")
	time.Sleep(2 * time.Second)
	fmt.Println("End Process 1")
}

func process2() {
	fmt.Println("Start Process 2")
	time.Sleep(2 * time.Second)
	fmt.Println("End Process 2")
}

func process3() {
	fmt.Println("Start Process 3")
	time.Sleep(2 * time.Second)
	fmt.Println("End Process 3")
}

func Run() {
	go process1()
	go process2()
	go process3()
	time.Sleep(3 * time.Second)
	fmt.Println("Done")
}
