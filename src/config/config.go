package config

import (
    "database/sql"
    _ "github.com/go-sql-driver/mysql"
    "gopkg.in/mgo.v2"
)

func GetMySQLDB() (db *sql.DB,err error) {
    dbDriver := "mysql"
    dbUser := "root"
    dbPass := ""
    dbName := "godemo"
    db, err = sql.Open(dbDriver, dbUser+":"+dbPass+"@/"+dbName)
    return  
}

func GetMongoDB () (*mgo.Database,error) {
    host :="mongodb://localhost:27017"
    dbName:="goDemo"

    session,err := mgo.Dial(host)

    if (err !=nil) {
        return nil,err
    }

    db:=session.DB(dbName)

    return db, nil
}

