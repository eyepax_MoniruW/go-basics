package basicauths

import (
	"../../../config"
	"../../../models"
	"net/http"
)

func BasicAuth(next http.Handler) http.Handler  {
	return http.HandlerFunc(func(response http.ResponseWriter,request *http.Request) {
		username,password,ok:=request.BasicAuth()
		if !ok ||!CheckUsernameAndPassword(username,password) {
			response.WriteHeader(http.StatusUnauthorized)
			response.Write([]byte("Unauthorized"))
		} else {
			next.ServeHTTP(response,request)
		}
	})
}

func CheckUsernameAndPassword(username,password string) bool  {
	db,err :=config.GetMongoDB()
	if err!=nil {
		return false
	} else {
		accountModel := models.AccountModel{
			Db: db,
		}
		return accountModel.CheckUsernameAndPassword(username,password)
	}
}

