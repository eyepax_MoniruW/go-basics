package api1

import (
	"fmt"
	"net/http"
)

func Hello(response http.ResponseWriter,request *http.Request)  {
	fmt.Fprint(response,"Hello World")
}

func Hi(response http.ResponseWriter,request *http.Request)  {
	fmt.Fprint(response,"Hi")
}
