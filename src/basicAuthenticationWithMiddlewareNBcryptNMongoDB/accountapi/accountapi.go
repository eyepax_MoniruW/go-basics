package accountapi

import (
	"../../config"
	"../../entities"
	"../../models"
	"encoding/json"
	"gopkg.in/mgo.v2/bson"
	"net/http"
)

func Create(response http.ResponseWriter,request *http.Request)  {
	db,err :=config.GetMongoDB()

	if err!=nil {
		respondWithError(response,http.StatusBadRequest,err.Error())
	} else {
		accountModel :=models.AccountModel{
			Db : db,
		}
		 var account entities.Account
		account.Id=bson.NewObjectId()
		err2:=json.NewDecoder(request.Body).Decode(&account)
		if err2 != nil {
			respondWithError(response,http.StatusBadRequest,err2.Error())
		} else {
			err3 :=accountModel.Create(&account)
			if err3 != nil {
				respondWithError(response,http.StatusBadRequest,err3.Error())
			} else {
				respondWithJson(response,http.StatusOK,account)
			}
		}
	}
}

func respondWithError(writer http.ResponseWriter, code int, msg string) {
	respondWithJson(writer,code, map[string]string{"error":msg})
}

func respondWithJson(writer http.ResponseWriter, code int, payload interface{}) {
	response,_:= json.Marshal(payload)
	writer.Header().Set("Content-type", "application/json")
	writer.WriteHeader(code)
	writer.Write(response)
}