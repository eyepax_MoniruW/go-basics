package basicAuthenticationWithMiddlewareNBcryptNMongoDB

import (
	"./accountapi"
	"./api1"
	"./middlewares/basicauths"
	"fmt"
	"github.com/gorilla/mux"
	"net/http"
)

func BasicAuthenticationWithMddlewareAndBcryptMongoDBDemo()  {
	router := mux.NewRouter()
	router.Handle("/api/account/create",basicauths.BasicAuth(http.HandlerFunc(accountapi.Create))).Methods("POST")
	router.Handle("/api/say/hello",basicauths.BasicAuth(http.HandlerFunc(api1.Hello))).Methods("GET")
	router.Handle("/api/say/hi",basicauths.BasicAuth(http.HandlerFunc(api1.Hi))).Methods("GET")

	err := http.ListenAndServe(":8000",router)

	if err != nil {
		fmt.Println(err)
	}
}