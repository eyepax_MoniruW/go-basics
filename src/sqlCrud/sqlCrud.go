package sqlCrud

import (
	"../config"
	"../entities"
	"../models"
	"encoding/json"
	"fmt"
	"github.com/gorilla/mux"
	"log"
	"net/http"
	"strconv"
)

func CrudDemo ()  {

	//init Router
	router := mux.NewRouter()

	//Router handlers /endpoints

	router.HandleFunc("/api/products",getProducts).Methods("GET")
	router.HandleFunc("/api/product",newProduct).Methods("POST")
	router.HandleFunc("/api/productName",updateProductName).Methods("PUT")
	router.HandleFunc("/api/product/{id}",deleteProduct).Methods("DELETE")

	log.Fatal(http.ListenAndServe(":8000",router))


}

//get list of products
func getProducts(w http.ResponseWriter, r *http.Request){

	db, err:= config.GetMySQLDB()
	if err != nil {
		fmt.Println(err)
	}else{
		productModel := models.ProductModelSQL{
			Db:db,
		}
		products, err := productModel.GetAllproducts()
		if err != nil {
			fmt.Println(err)
		} else{
			w.Header().Set("Content-Type","application/json ")
			json.NewEncoder(w).Encode(products)

		}
	}
}

//add new product
func newProduct(w http.ResponseWriter, r *http.Request){
	db, err:= config.GetMySQLDB()
	if err != nil {
		fmt.Println(err)
	}else{
		w.Header().Set("Content-Type","application/json ")

		decoder := json.NewDecoder(r.Body)
		var productInfo entities.ProductSQL
		err := decoder.Decode(&productInfo)
		if err != nil {
			panic(err)
		}
		log.Println(productInfo)

		productModel := models.ProductModelSQL{
			Db:db,
		}

		err = productModel.CreateNewProduct(&productInfo)

		if err!=nil{
			fmt.Print(err)
		} else{
			fmt.Println("Product Inserted Successfully ")
		}
	}
}

//update Product Name
func updateProductName(w http.ResponseWriter, r *http.Request){
	db, err:= config.GetMySQLDB()
	if err != nil {
		fmt.Println(err)
	}else{
		w.Header().Set("Content-Type","application/json ")

		decoder := json.NewDecoder(r.Body)
		var productName entities.ProductSQL
		err := decoder.Decode(&productName)
		if err != nil {
			panic(err)
		}
		log.Println(productName)

		productModel := models.ProductModelSQL{
			Db:db,
		}

		err = productModel.UpdateProductName(&productName)

		if err!=nil{
			fmt.Print(err)
		} else{
			fmt.Println("Product Name Updated Successfully ")
		}
	}
}

//Delete Product
func deleteProduct(w http.ResponseWriter, r *http.Request){
	db, err:= config.GetMySQLDB()
	if err != nil {
		fmt.Println(err)
	}else{
		w.Header().Set("Content-Type","application/json ")
		params:=mux.Vars(r)

		log.Println(params["id"])
		var idx=params["id"]

		n,err := strconv.ParseInt(idx,10,64)

		productModel := models.ProductModelSQL{
			Db:db,
		}

		err = productModel.DeleteProduct(n)

		if err!=nil{
			fmt.Print(err)
		} else{
			fmt.Println("Product Deleted Successfully ")
		}
	}
}

