package marshalAndUnMarshalJSON

import (
	"../entities"
	"encoding/json"
	"fmt"
)

func MarshalUnmarshalDemo()  {
	fmt.Println("---Marshal and Unmarshal Json---")
	//Demo1()
	//Demo2()
	Demo3()
	Demo4()
	Demo5()
	Demo6()
}

//// When category is not in the product struct

//func Demo1()  {
//	fmt.Println("---Demo 1---Marshal JSON without category entity in the struct---")
//	product :=entities.ProductMarshalUnmarshalJson{"p1","product 1", 2,5,10}
//	result, err :=json.Marshal(product)
//	if err !=nil {
//		fmt.Println(err)
//	} else {
//		fmt.Println(string(result))
//	}
//}

func Demo2()  {
	fmt.Println("---Demo 2---UnMarshal JSON---")
	var str string =`{
		"id":"p1",
		"name":"product 1",
		"price":2,
		"quantity":5,
		"status":10
	}`
	var product entities.ProductMarshalUnmarshalJson
	json.Unmarshal([]byte(str),&product)
	fmt.Println("Product Info")
	fmt.Println(product.ToString())
}

func Demo3()  {
	fmt.Println("---Demo 2---Marshal JSON with category in the struct---")
	product:=entities.ProductMarshalUnmarshalJson{
		Id:       "p1",
		Name:     "product 1",
		Price:    2,
		Quantity: 5,
		Status:   10,
		Category: entities.Category{
			Id:   "c1",
			Name: "category 1",
		},
	}
	result, err :=json.Marshal(product)
	if err !=nil {
		fmt.Println(err)
	} else {
		fmt.Println(string(result))
	}
}

func Demo4()  {
	fmt.Println("---Demo 4---Unmarshal with category in the struct ---")
	var str string =`{"id":"p1","name":"product 1","price":2,"quantity":5,"status":10,"category":{"id":"c1","name":"category 1"}}`
	var product entities.ProductMarshalUnmarshalJson
	json.Unmarshal([]byte(str),&product)
	fmt.Println("Product Info")
	fmt.Println(product.ToString())
	fmt.Println("Category Info")
	fmt.Println("\tCategory Id: ",product.Category.Id)
	fmt.Println("\tCategory Name: ",product.Category.Name)
}

func Demo5()  {
	fmt.Println("---Demo 5---Marshal JSON with category,Comment---")
	product:=entities.ProductMarshalUnmarshalJson{
		Id:       "p1",
		Name:     "product 1",
		Price:    2,
		Quantity: 5,
		Status:   10,
		Category: entities.Category{
			Id:   "c1",
			Name: "category 1",
		},
		Comments: []entities.Comment{
			entities.Comment{
				Title:   "Title 1",
				Content: "Content 1",
			},
			entities.Comment{
				Title:   "Title 2",
				Content: "Content 2",
			},
			entities.Comment{
				Title:   "Title 3",
				Content: "Content 3",
			},
		},
	}
	result, err :=json.Marshal(product)
	if err !=nil {
		fmt.Println(err)
	} else {
		fmt.Println(string(result))
	}
}

func Demo6()  {
	fmt.Println("---Demo 6---Unmarshal with category in the struct ---")
	var str string =`{"id":"p1","name":"product 1","price":2,"quantity":5,"status":10,
	"category":{"id":"c1","name":"category 1"},
	"comments":[{"title":"Title 1","content":"Content 1"},{"title":"Title 2","content":"Content 2"},{"title":"Title 3","content":"Content 3"}]}`
	var product entities.ProductMarshalUnmarshalJson
	json.Unmarshal([]byte(str),&product)
	fmt.Println("Product Info")
	fmt.Println(product.ToString())
	fmt.Println("Category Info")
	fmt.Println("\tCategory Id: ",product.Category.Id)
	fmt.Println("\tCategory Name: ",product.Category.Name)
	fmt.Println("Comment List")
	for _,comment :=range product.Comments{
		fmt.Println("\t",comment.ToString())
		fmt.Println("\t-------------------")
	}
}
