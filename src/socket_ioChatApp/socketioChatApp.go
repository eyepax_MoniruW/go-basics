package socket_ioChatApp

import (
	"fmt"
	socketio "github.com/googollee/go-socket.io"
	"net/http"
)

func SocketIoChatApp()  {
	server,err :=socketio.NewServer(nil)
	if err != nil {
		fmt.Println(err)
	} else {

		server.OnConnect("/", func(s socketio.Conn) error {
			s.SetContext("")
			fmt.Println("connected:", s.ID())
			return nil
		})


		http.HandleFunc("/", func(response http.ResponseWriter, request *http.Request) {
			http.ServeFile(response,request,"views/chat/index.html")
		})

		err2 :=http.ListenAndServe(":3000",nil)

		if err2 != nil {
			fmt.Println(err2)
		} else {
			fmt.Println("Serving at http://localhost:3000.....")
		}
	}
}