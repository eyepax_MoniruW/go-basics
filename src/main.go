package main

import (
	//"./sqlCrud"
	//"./mongoCrud"
	//"./multiParameters"
	//"./pointers"
	//"./struct"
	//"./dateAndTime"
	//"./polymorphism"
	//"./marshalAndUnMarshalJSON"
	//"./middleware"
	//"./basicAuthenticationWithMiddleware"
	//"./basicAuthenticationWithMiddlewareNBcryptNMongoDB"
	//"./securingRestfulApiJWT"
	//"./socket_ioChatApp"
	"./goRoutines"

)

func main ()  {

	//sqlCrud.CrudDemo()
	//mongoCrud.MongoCrud()
	//multiParameters.MultiSum(1,4,5,8,6,78)
	//pointers.PointersDemo()
	//_struct.StructDemo()
	//dateAndTime.DateAndTimeDemo()
	//polymorphism.PolymorphismDemo()
	//marshalAndUnMarshalJSON.MarshalUnmarshalDemo()
	//middleware.MiddlewareDemo()
	//basicAuthenticationWithMiddleware.BasicAuthenticationMiddlewareDemo()
	//basicAuthenticationWithMiddlewareNBcryptNMongoDB.BasicAuthenticationWithMddlewareAndBcryptMongoDBDemo()
	//securingRestfulApiJWT.SecuringRestfulApiWitjJWT()
	//socket_ioChatApp.SocketIoChatApp()
	goRoutines.GoRoutineDemo()

}


