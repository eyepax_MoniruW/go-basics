package models

import (
	"gopkg.in/mgo.v2"
	"../entities"
	"gopkg.in/mgo.v2/bson"
)

type ProductModelMongo struct {
	Db *mgo.Database
	Collection string
}

func (productModel ProductModelMongo) FindAll() (products []entities.ProductMongo,err error) {
	err = productModel.Db.C(productModel.Collection).Find(bson.M{}).All(&products)
	return
}

func (productModel ProductModelMongo) Find(id string) (product entities.ProductMongo,err error) {
	err = productModel.Db.C(productModel.Collection).FindId(bson.ObjectIdHex(id)).One(&product)
	return
}

func (productModel ProductModelMongo) Create(product *entities.ProductMongo) error {
	err := productModel.Db.C(productModel.Collection).Insert(&product)
	return err
}

func (productModel ProductModelMongo) Update(product *entities.ProductMongo) error {
	err := productModel.Db.C(productModel.Collection).UpdateId(product.Id, &product)
	return err
}

func (productModel ProductModelMongo) Delete(product entities.ProductMongo) error {
	err := productModel.Db.C(productModel.Collection).Remove(product)
	return err
}