package models

import (
	"../entities"
	"database/sql"
)

type ProductModelSQL struct{
	Db *sql.DB 
}

func (productModel ProductModelSQL) GetAllproducts() ([]entities.ProductSQL,error) {
	rows,err := productModel.Db.Query("SELECT * FROM product")
	if err != nil {
		return nil,err
	} else {
		products := []entities.ProductSQL{}
		for rows.Next(){
			var id int64
			var name string
			var price int64
			var quantity int64
			var status int64

			err2 := rows.Scan(&id,&name,&price,&quantity,&status)
			if err2 != nil{
				return nil,err2
			} else {
				product := entities.ProductSQL{id, name,price,quantity,status}
				products = append(products,product)
			}
		}
		return products,nil
	}
}

func (productModel ProductModelSQL) CreateNewProduct(product *entities.ProductSQL) error {
	result,err := productModel.Db.Exec("INSERT INTO product (id,name,price,quantity,status) values (?,?,?,?,?)",
		product.Id,product.Name,product.Price,product.Quantity,product.Status)
	if err !=nil {
		return err
	}else{
		product.Id ,_=result.LastInsertId()
		return nil
	}
}


func (productModel ProductModelSQL) UpdateProductName(productName *entities.ProductSQL) error {
	result,err := productModel.Db.Exec("UPDATE product SET name =? WHERE id=? ",productName.Name,productName.Id)
	if err !=nil {
		return err
	}else{
		_=result
		return nil
	}
}

func (productModel ProductModelSQL) DeleteProduct(id int64) error {
	result,err := productModel.Db.Exec("DELETE FROM product WHERE id=? ",id)
	if err !=nil {
		return err
	}else{
		_=result
		return nil
	}
}
