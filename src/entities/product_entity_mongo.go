package entities

import "gopkg.in/mgo.v2/bson"

type ProductMongo struct{
	Id bson.ObjectId `json:"_id,omitempty" bson:"_id,omitempty"`
	Name string `bson:"name"`
	Price int64 `bson:"price"`
	Quantity int64 `bson:"quantity"`
	Status int64 `bson:"status"`
}