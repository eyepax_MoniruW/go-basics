package entities

import "math"

type Circle struct {
	Name string
	R float32
}

func (circle Circle) Area() float32  {
	return math.Pi*circle.R*circle.R
}
func (circle Circle) Perimeter() float32  {
	return 2*math.Pi*circle.R
}

func (circle Circle) Type() string  {
	return circle.Name
}
