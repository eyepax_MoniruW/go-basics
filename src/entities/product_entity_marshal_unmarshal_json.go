package entities

import "fmt"

type ProductMarshalUnmarshalJson struct {
	Id string `json:"id"`
	Name string `json:"name"`
	Price int `json:"price"`
	Quantity int `json:"quantity"`
	Status int `json:"status"`
	Category Category `json:"category"`
	Comments []Comment `json:"comments"`
}

func (product ProductMarshalUnmarshalJson) ToString() string  {
	return fmt.Sprintf(
		"Id: %s\nName: %s\nPrice: %d\nQuantity: %d\nstatus:%d",
		product.Id, product.Name, product.Price, product.Quantity, product.Status)

}
