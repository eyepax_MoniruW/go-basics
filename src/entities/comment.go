package entities

import "fmt"

type Comment struct {
	Title string `json:"title"`
	Content string `json:"content"`
}

func (comment Comment) ToString() string  {
	return fmt.Sprintf("Title: %s\n\tContent: %s",comment.Title,comment.Content)

}
