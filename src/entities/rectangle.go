package entities

type Rectangle struct {
	Name string
	A float32
	B float32
}

func (rectangle Rectangle) Area() float32  {
	return rectangle.A*rectangle.B
}
func (rectangle Rectangle) Perimeter() float32  {
	return 2*(rectangle.A+rectangle.B)
}

func (rectangle Rectangle) Type() string  {
	return rectangle.Name
}
