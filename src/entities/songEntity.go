package entities

type Song struct {
	Id int
	Name string
	Melody Melody
}
