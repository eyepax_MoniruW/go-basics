package entities

import "fmt"

type Product struct {
	Id,Name string
	Price int64
	Quantity int64
}

func (product Product) ToString() string  {
	return fmt.Sprintf("Id: %s\nName: %s\nPrice: %d\nQuantity: %d",
		product.Id,product.Name,product.Price,product.Quantity)
}