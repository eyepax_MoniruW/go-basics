package main

import (
	"./entities"
	"bufio"
	"fmt"
	"io/ioutil"
	"os"
	"strconv"
	"strings"
)

func main()  {
	Demo1()
	Demo2()
	//Demo3()
	//Demo4()
	//Demo5()
	//Demo6()
	//Demo7()
	//Demo8()
}

func Demo1()  {
	file,err :=os.Create("data/a.txt")

	if err!= nil {
		fmt.Print(err)
	} else {
		fmt.Println(file)
		fmt.Println("Done")
	}
	file.Close()
}

func Demo2()  {
	file,err :=os.Stat("data/a.txt")

	if err!=nil {
		fmt.Println(err)
	} else {
		fmt.Println("Name: ",file.Name())
		fmt.Println("Size(bytes): ",file.Size())
		fmt.Println("Permission: ",file.Mode())
		fmt.Printf("Perm: %04o\n",file.Mode().Perm())
		fmt.Println("ModTime: ",file.ModTime())
	}
}

func Demo3()  {
	err :=os.Remove("data/a.txt")
	if err != nil {
		fmt.Println(err)
	}
}

func Demo4()  {
	bytes,err := ioutil.ReadFile("data/a.txt")

	if err != nil {
		fmt.Println(err)
	} else {
		fmt.Println(string(bytes))
	}
}

func Demo5()  {
	file,err := os.Open("data/a.txt")
	if err != nil {
		fmt.Println(err)
	} else {
		Scanner :=bufio.NewScanner(file)
		for Scanner.Scan() {
			line :=Scanner.Text()
			fmt.Println(line)
		}
	}
	file.Close()
}

func Demo6()  {
	file,err :=os.Create("data/c.txt")
	if err!= nil {
		fmt.Println(err)
	} else {
		file.WriteString("My File c")
	}
	file.Close()
}

func Demo7()  {
	file,err := os.OpenFile("data/b.txt",os.O_APPEND| os.O_WRONLY,0064)
	if err != nil {
		fmt.Println(err)
	} else {
		fmt.Fprintln(file,"Line 5")
		fmt.Fprintln(file,"Line 6")
	}
	file.Close()
}

func Demo8()  {
	file,err := os.Open("data/products.csv")
	if err != nil {
		fmt.Println(err)
	} else {
		var products []entities.Product
		Scanner :=bufio.NewScanner(file)
		for Scanner.Scan() {
			line :=Scanner.Text()
			result :=strings.Split(line,",")
			price,_ :=strconv.ParseInt(result[2],10,64)
			quantity,_ :=strconv.ParseInt(result[2],10,64)
			products=append(products,entities.Product{
				Id:       result[0],
				Name:     result[1],
				Price:    price,
				Quantity: quantity,
			})
		}
		fmt.Println("Product List from CSV File ")
		for _,product:= range products {
			fmt.Println(product.ToString() )
			fmt.Println("--------------------")
		}
	}
	file.Close()
}